﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Raytracing2D
{
    public partial class Form1 : Form
    {
        bool Pause = false;

        int ColumnSize;

        const int PerspectiveLength = 500;
        int MaxLength = 500;
        int RaysCount = 700;
        double ViewArea = Math.PI / 6;
        double RotationAngle = 0.03;

        double ViewAngle = 0;

        double[] RayAngles;
        double[] RayLengths;
        double[] ZLengths;
        List<IGameObject> GameObjects;

        PointF UserLocation;

        Pen WhitePen,
            RedPen,
            BlackPen;
        SolidBrush fillBrush = new SolidBrush(Color.FromArgb(100, 255, 255, 200));
        Bitmap CanvasMap;
        Graphics MapGraphics;

        Bitmap CanvasPerson;
        Graphics PersonGraphics;

        Timer T;
        Random R = new Random(42);
        Point MouseLocation = new Point(0, 0);

        bool ShowRays
        {
            get => checkRays.Checked;
        }
        bool ShowObjects
        {
            get => checkObjects.Checked;
        }
        bool ShowArea
        {
            get => checkArea.Checked;
        }
        bool TexturesEnabled
        {
            get => checkTextures.Checked;
        }

        bool GoUp, GoDown, GoLeft, GoRight;

        public Form1()
        {
            InitializeComponent();

            this.pictureBox2.MouseMove += FirstPersonMouseMove;
            this.KeyDown += KeyPressed;
            this.KeyUp += KeyReleased;
            this.SizeChanged += ChangingSize;

            CanvasMap = new Bitmap(pictureBox1.Width * 2, pictureBox1.Height * 2);
            pictureBox1.Image = CanvasMap;
            MapGraphics = Graphics.FromImage(CanvasMap);

            CanvasPerson = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            pictureBox2.Image = CanvasPerson;
            PersonGraphics = Graphics.FromImage(CanvasPerson);

            WhitePen = new Pen(Color.FromArgb(255, 255, 255, 255), 1);
            RedPen = new Pen(Color.Red, 2);
            BlackPen = new Pen(Color.Black, 1);

            SetRayCount(CanvasPerson.Width + 1);
            SetViewArea(30);

            RayAngles = new double[RaysCount];
            RayLengths = new double[RaysCount];
            ZLengths = new double[RaysCount];
            InitializeGameObjects();

            SetRayLength(MaxLength);
            trackRayLength.Value = MaxLength;
            trackRayLength.Maximum = PerspectiveLength;
            label10.Text = PerspectiveLength.ToString();

            SetRays();

            T = new Timer();
            T.Interval = 100;
            T.Tick += UpdateUser;
            T.Start();
        }

        private void ChangingSize(object sender, EventArgs e)
        {
            CanvasPerson = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            pictureBox2.Image = CanvasPerson;
            PersonGraphics = Graphics.FromImage(CanvasPerson);
        }

        private void FirstPersonMouseMove(object sender, MouseEventArgs e)
        {
            this.pictureBox2.MouseMove -= FirstPersonMouseMove;

            if (e.X > CanvasPerson.Width / 2)
                ViewAngle += RotationAngle;
            else if (e.X < CanvasPerson.Width / 2)
                ViewAngle -= RotationAngle;

            ViewAngle = ViewAngle % (2 * Math.PI);

            Cursor.Position = new Point(CanvasPerson.Width / 2 + Location.X + 11, CanvasPerson.Height / 2 + Location.Y);

            this.pictureBox2.MouseMove += FirstPersonMouseMove;
        }

        private void MoveMouse(object sender, MouseEventArgs e)
        {
            MouseLocation = e.Location;
        }

        private void KeyReleased(object sender, KeyEventArgs e)
        {
            SetKey(e.KeyCode, false);
        }

        private void KeyPressed(object sender, KeyEventArgs e)
        {
            SetKey(e.KeyCode, true);
        }

        void SetKey(Keys K, bool Value)
        {
            switch (K)
            {
                case Keys.W:
                    GoUp = Value;
                    break;
                case Keys.S:
                    GoDown = Value;
                    break;
                case Keys.A:
                    GoLeft = Value;
                    break;
                case Keys.D:
                    GoRight = Value;
                    break;
                case Keys.Escape:
                    Pause = Value ? Pause : !Pause;
                    break;
                default:
                    break;
            }
        }

        void MoveUser()
        {
            PointF newLoc = new PointF(UserLocation.X, UserLocation.Y);
            int Step = 2;
            if (GoUp)
            {
                newLoc.X = (float)(UserLocation.X + Step * Math.Sin(ViewAngle));
                newLoc.Y = (float)(UserLocation.Y - Step * Math.Cos(ViewAngle));
            }
            else if (GoDown)
            {
                newLoc.X = (float)(UserLocation.X - Step * Math.Sin(ViewAngle));
                newLoc.Y = (float)(UserLocation.Y + Step * Math.Cos(ViewAngle));
            }
            if (GoLeft)
            {
                ViewAngle -= Math.PI / 2;
                newLoc.X = (float)(newLoc.X + Step * Math.Sin(ViewAngle));
                newLoc.Y = (float)(newLoc.Y - Step * Math.Cos(ViewAngle));
                ViewAngle += Math.PI / 2;
            }
            else if (GoRight)
            {
                ViewAngle += Math.PI / 2;
                newLoc.X = (float)(newLoc.X + Step * Math.Sin(ViewAngle));
                newLoc.Y = (float)(newLoc.Y - Step * Math.Cos(ViewAngle));
                ViewAngle -= Math.PI / 2;
            }

            //if (UserLocation.X < 0)
            //    UserLocation.X = 0;
            //if (UserLocation.X > CanvasMap.Width)
            //    UserLocation.X = CanvasMap.Width;

            //if (UserLocation.Y < 0)
            //    UserLocation.Y = 0;
            //if (UserLocation.Y > CanvasMap.Height)
            //    UserLocation.Y = CanvasMap.Height;

            bool AllowToMove = true;
            foreach (var l in GameObjects)
            {
                if (l.IsIntersected(UserLocation, newLoc))
                {
                    AllowToMove = false;
                    break;
                }
            }
            if (AllowToMove)
                UserLocation = newLoc;
        }

        void GetViewAngle()
        {
            ViewAngle = Math.Asin((MouseLocation.X - UserLocation.X)
                / Math.Sqrt(Math.Pow(UserLocation.Y - MouseLocation.Y, 2)
                + Math.Pow((MouseLocation.X - UserLocation.X), 2)));

            if (MouseLocation.Y >= UserLocation.Y)
            {
                ViewAngle *= -1;
                ViewAngle += Math.PI;
            }
        }

        private void UpdateUser(object sender, EventArgs e)
        {
            ChangeAngles();
            if(!Pause && (GoUp || GoDown || GoLeft || GoRight))
                MoveUser();
            SetRays();
            DrawFirstPerson();


            pictureBox2.MouseMove -= FirstPersonMouseMove;
            if (!Pause)
            {
                pictureBox2.MouseMove += FirstPersonMouseMove;
                panelMenu.Enabled = false;
                panelMenu.Visible = false;
                pictureBox2.Focus();
                Cursor = Cursors.Cross;

            }
            else
            {
                PersonGraphics.DrawString("PAUSE", new Font(FontFamily.Families[0], 25, FontStyle.Bold), new SolidBrush(Color.Red), CanvasPerson.Width / 2 - 30, CanvasPerson.Height / 4);

                panelMenu.Enabled = true;
                panelMenu.Visible = true;
                Cursor = Cursors.Default;
                panelMenu.Focus();
            }
        }

        void InitializeGameObjects()
        {
            GameObjects = new List<IGameObject>();

            GameObjects.Add(new InverseCircle(new PointF(CanvasMap.Width / 2, CanvasMap.Height / 2), CanvasMap.Width / 2));

            for (int i = 0; i < 3; i++)
                GameObjects.Add(
                    new Wall(new PointF(R.Next(CanvasMap.Width), R.Next(CanvasMap.Height)),
                        new PointF(R.Next(CanvasMap.Width), R.Next(CanvasMap.Height))));

            for (int i = 0; i < 2; i++)
                GameObjects.Add(new Circle(new PointF(R.Next(CanvasMap.Width), R.Next(CanvasMap.Height)), R.Next(50) + 50));

            
            while (GetMinDistance(UserLocation) <= 0)
                UserLocation = new PointF(R.Next(CanvasMap.Width), R.Next(CanvasMap.Height));

        }


        PointF GetEndPoint(PointF Start, double Length, double Angle)
            => new PointF(
                                (float)(Start.X + Length * Math.Sin(Angle)),
                                (float)(Start.Y - Length * Math.Cos(Angle)));
        void SetRays()
        {
            MapGraphics.FillRectangle(new SolidBrush(Color.Black), 0, 0, CanvasMap.Width, CanvasMap.Height);

            ChangeAngles();

            double MinDistance = GetMinDistance(UserLocation);
            if (MinDistance >= MaxLength)
            {
                for (int i = 0; i < RayLengths.Length; i++)
                    RayLengths[i] = MaxLength;
            }
            else
            {
                PointF TempLocation;
                double tempDistance;
                for (int i = 0; i < RayLengths.Length; i++)
                {
                    RayLengths[i] = 0;
                    tempDistance = MinDistance;

                    while ( RayLengths[i] < MaxLength)
                    {
                        RayLengths[i] += tempDistance;

                        TempLocation = GetEndPoint(UserLocation, RayLengths[i], RayAngles[i]);
                        tempDistance = GetMinDistance(TempLocation);
                        if (tempDistance <= 0)
                            break;
                    }
                    if (RayLengths[i] > MaxLength)
                        RayLengths[i] = MaxLength;

                    ZLengths[i] = RayLengths[i] * Math.Abs(Math.Cos(RayAngles[i] - ViewAngle));
                }
            }

            DrawMinimap();
            if (ShowObjects)
                DrawGameObjects();
        }

        void DrawMinimap()
        {
            PointF Prev = new PointF(
                            (int)(UserLocation.X + RayLengths[0] * Math.Sin(RayAngles[0])),
                            (int)(UserLocation.Y - RayLengths[0] * Math.Cos(RayAngles[0]))
                            );
            PointF Next;
            for (int i = 1; i < RayLengths.Length; i++)
            {
                Next = new PointF(
                            (int)(UserLocation.X + RayLengths[i] * Math.Sin(RayAngles[i])),
                            (int)(UserLocation.Y - RayLengths[i] * Math.Cos(RayAngles[i]))
                            );
                if (ShowArea)
                    MapGraphics.FillPolygon(fillBrush, new PointF[] { UserLocation, Prev, Next });
                if (ShowRays)
                    MapGraphics.DrawLine(WhitePen, UserLocation, Next);
                Prev = Next;
            }
            pictureBox1.Invalidate();
        }

        void DrawGameObjects()
        {
            foreach (var l in GameObjects)
                l.Draw2D(ref MapGraphics);
            pictureBox1.Invalidate();
        }

        double GetMinDistance(PointF Location) => GetClosestObject(Location).distance;

        (IGameObject Obj, double distance) GetClosestObject(PointF Location)
        {
            double Min = Double.MaxValue;
            IGameObject O = GameObjects[0];
            double d;
            foreach (var l in GameObjects)
            {
                d = Math.Round(l.Distance(Location), 2);
                if (d < Min)
                {
                    Min = d;
                    O = l;
                }
            }
            return (O, Min);
        }

        private void RayCountChanging(object sender, EventArgs e)
        {
            SetRayCount(trackRayCount.Value);
        }
        void SetRayCount(int value)
        {
            RaysCount = value;
            RayLengths = new double[RaysCount];
            RayAngles = new double[RaysCount];
            ZLengths = new double[RaysCount];
            label7.Text = RaysCount.ToString();
        }

        private void ViewAreaChanging(object sender, EventArgs e)
        {
            SetViewArea(trackViewArea.Value);
        }
        void SetViewArea(int value)
        {
            ViewArea = (double)value / 180 * Math.PI;
            label8.Text = value.ToString();
        }

        private void RayLengthChanging(object sender, EventArgs e)
        {
            SetRayLength(trackRayLength.Value);
        }
        void SetRayLength(int value)
        {
            MaxLength = value;
            label9.Text = MaxLength.ToString();
        }

        double DistanceToUser(PointF Location) => Math.Sqrt(Math.Pow(UserLocation.X - Location.X, 2) + Math.Pow(UserLocation.Y - Location.Y, 2));

        void ChangeAngles()
        {
            double Step = ViewArea / (RaysCount - 1);
            if (!double.IsNaN(ViewAngle))
                for (int i = 0; i < RaysCount; i++)
                    RayAngles[i] = i * Step + ViewAngle - ViewArea / 2;
        }

        double DistanceToRow((Point, Point) Line, Point TempLocation) => Math.Abs(((Line.Item1.Y - Line.Item2.Y) * TempLocation.X
            + (Line.Item2.X - Line.Item1.X) * TempLocation.Y
            + Line.Item1.X * Line.Item2.Y - Line.Item1.Y * Line.Item2.X)
            / Math.Sqrt(Math.Pow(Line.Item1.X - Line.Item2.X, 2) + Math.Pow(Line.Item1.Y - Line.Item2.Y, 2)));

        private void buttonResetLines_Click(object sender, EventArgs e)
        {
            InitializeGameObjects();
        }

        void DrawFirstPerson()
        {
            float Step = (float)CanvasPerson.Width / (RaysCount - 1);
            ColumnSize = (int)(CanvasPerson.Height * 0.1);
            float ViewHeight = 0;// (float)MaxLength / PerspectiveLength * CanvasPerson.Height;
            int color;

            PersonGraphics.Clear(Color.Black);
            //PersonGraphics.DrawImage(Properties.Resources.Floor, 0, CanvasPerson.Height - ViewHeight, CanvasPerson.Width, ViewHeight);
            //PersonGraphics.FillRectangle(new SolidBrush(Color.FromArgb(0, 0, 100)), 0, 0, CanvasPerson.Width, CanvasPerson.Height / 2);
            //PersonGraphics.FillRectangle(new SolidBrush(Color.FromArgb(0, 50, 0)), 0, CanvasPerson.Height / 2, CanvasPerson.Width, CanvasPerson.Height / 2);

            ViewHeight = (float)MaxLength / PerspectiveLength * CanvasPerson.Height * 0.65f;
            ViewHeight = ViewHeight > CanvasPerson.Height / 2 ? CanvasPerson.Height / 2 : ViewHeight;
            for (int i = 0; i < ViewHeight; i++)
            {
                color = (int)( (float)i/ ViewHeight * 255);
                //color = (int)((float)i * 2 / CanvasPerson.Height * MaxLength / PerspectiveLength * 255);
                PersonGraphics.FillRectangle(
                    new SolidBrush(Color.FromArgb(
                    color, 200, 200, 200)),
                    0, CanvasPerson.Height - ViewHeight + i, CanvasPerson.Width, 1);
            }

            for (int i = 0; i < RayLengths.Length; i++)
            {
                ViewHeight = (float)(ColumnSize * (/*MaxLength*/PerspectiveLength / ZLengths[i]));
                ViewHeight = (ViewHeight > CanvasPerson.Height) ? (float)CanvasPerson.Height : ViewHeight;

                (IGameObject Temp, double Distance) = GetClosestObject(GetEndPoint(UserLocation, RayLengths[i], RayAngles[i]));

                if (Distance < 1)
                {
                    if (TexturesEnabled)
                    {
                        Bitmap bmp = Temp.GetSprite(GetEndPoint(UserLocation, RayLengths[i], RayAngles[i]));
                        bmp = new Bitmap(bmp, (int)(Step * 2), (int)ViewHeight);

                        PersonGraphics.DrawImage(bmp, i * Step,
                           (float)(0.5 * (CanvasPerson.Height - ViewHeight)));

                        color = (int)(255 * RayLengths[i] * Math.Cos(RayAngles[i] - ViewAngle) / MaxLength);
                        color = (color > 255) ? 255 : color;
                        PersonGraphics.FillRectangle(
                                   new SolidBrush(
                                       Color.FromArgb(color, 0, 0, 0)),
                                   i * Step,
                                   (float)(0.5 * (CanvasPerson.Height - ViewHeight)),
                                   Step,
                                   ViewHeight);
                    }
                    else
                    {
                        color = (int)(255 - 255 * RayLengths[i] * Math.Cos(RayAngles[i] - ViewAngle) / MaxLength);
                        color = (color > 255) ? 255 : color;
                        PersonGraphics.FillRectangle(
                                   new SolidBrush(
                                       Color.FromArgb(color, color, color)),
                                   i * Step,
                                   (float)(0.5 * (CanvasPerson.Height - ViewHeight)),
                                   Step,
                                   ViewHeight);
                    }
                }
            }
            pictureBox2.Invalidate();

            //PersonGraphics.DrawImage(Properties.Resources.Fist_Right,, 0, CanvasPerson.Height - Properties.Resources.Fist_Right.Height);
        }
    }
}
