﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Raytracing2D
{

    public interface IGameObject
    {
        bool IsIntersected(PointF A, PointF B);
        double Distance(PointF A);
        Bitmap GetSprite(PointF Intersection); 
        void Draw(ref Graphics G, PointF IntersectedLoc);
        void Draw2D(ref Graphics G);
    }

    public class Wall : IGameObject
    {

        Bitmap Sprite = Properties.Resources.Wall;
        Bitmap[] Sprites;

        public PointF A
        {
            get => a;
        }
        public PointF B
        {
            get => b;
        }

        PointF a, b;
        public Wall(PointF A, PointF B)
        {
            a = A;
            b = B;

            Sprites = new Bitmap[Sprite.Width];
            for (int i = 0, j; i < Sprites.Length; i++)
            {
                Sprites[i] = new Bitmap(1, Sprite.Height);
                for (j = 0; j < Sprite.Height; j++)
                    Sprites[i].SetPixel(0, j, Sprite.GetPixel(i, j));
            }
        }

        public bool IsIntersected(PointF A, PointF B)
        {
            double multiply(float ax, float ay, float bx, float by) => ax * by - bx * ay;

            var v1 = multiply(b.X - a.X, b.Y - a.Y, A.X - a.X, A.Y - a.Y);
            var v2 = multiply(b.X - a.X, b.Y - a.Y, B.X - a.X, B.Y - a.Y);
            var v3 = multiply(B.X - A.X, B.Y - A.Y, a.X - A.X, a.Y - A.Y);
            var v4 = multiply(B.X - A.X, B.Y - A.Y, b.X - A.X, b.Y - A.Y);
            return (v1 * v2 <= 0 && v3 * v4 <= 0);
        }

        public PointF GetIntersection(PointF A, PointF B)
        {
            if (IsIntersected(A, B))
            {
                double x = (b.X * a.Y - a.X * b.Y + (A.X * B.Y - B.X * A.Y) * (b.X - a.X) / (B.X - A.X)) / (a.Y - b.Y + (b.X - a.X) * (B.Y - A.Y) / (B.X - A.X));
                    //((a.Y - b.Y) + (b.X - a.X) * (B.X * A.Y - A.X * B.Y) / (B.X - A.X)) / (b.X * a.Y - a.X * b.Y + (b.X - a.Y) * (B.Y - A.Y) / (B.X - A.X));

                double y = (B.X * A.Y - A.X * B.Y + x * (B.Y - A.Y)) / (B.X - A.X);

                return new PointF((float)x, (float)y);
            }
            throw new NotSupportedException("Lines do not cross.");
        }

        public double Distance(PointF Location)
        {
            double t = ((Location.X - a.X) * (b.X - a.X)
                + (Location.Y - a.Y) * (b.Y - a.Y))
              / (Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));

            t = (t > 1) ? 1 : (t < 0) ? 0 : t;

            return Math.Sqrt(
                Math.Pow(a.X - Location.X + (b.X - a.X) * t, 2)
                + Math.Pow(a.Y - Location.Y + (b.Y - a.Y) * t, 2)
                );
        }

        public void Draw(ref Graphics G, PointF IntersectedLoc)
        {

        }

        public void Draw2D(ref Graphics G)
        {
            G.DrawLine(new Pen(Color.Red, 2), a, b);
        }

        public Bitmap GetSprite(PointF Intersection)
        {
            int Index = (int)(Math.Sqrt(Math.Pow(Intersection.X - a.X, 2) + Math.Pow(Intersection.Y - a.Y, 2)) * 10);
            return Sprites[Math.Abs(Index % Sprites.Length)];
        }
    }

    public class Circle : IGameObject
    {
        protected Bitmap Sprite = Properties.Resources.Wall;
        protected Bitmap[] Sprites;
        protected float Width;
        protected float Angle;

        public PointF Center { get; }

        public float Radius { get; }

        public Circle(PointF Center, float Radius)
        {
            this.Center = Center;
            this.Radius = Radius;

            Width = (float)(2 * Math.PI * Radius);

            Sprites = new Bitmap[Sprite.Width];
            for (int i = 0, j; i < Sprites.Length; i++)
            {
                Sprites[i] = new Bitmap(1, Sprite.Height);
                for (j = 0; j < Sprite.Height; j++)
                    Sprites[i].SetPixel(0, j, Sprite.GetPixel(i, j));
            }
        }

        public bool IsIntersected(PointF A, PointF B) => Math.Sign(Distance(A)) != Math.Sign(Distance(B));

        public virtual double Distance(PointF Location) => Math.Sqrt(Math.Pow(Location.X - Center.X, 2) + Math.Pow(Location.Y - Center.Y, 2)) - Radius;

        public void Draw(ref Graphics G, PointF IntersectedLoc)
        {

        }

        public Bitmap GetSprite(PointF Intersection)
        {
            PointF Centred = new PointF(Intersection.X - Center.X, Intersection.Y - Center.Y);
            double a = Math.Sqrt(Centred.X * Centred.X + Centred.Y * Centred.Y) * Radius;
            double b = Centred.Y * -Radius;
            int Index = (int)(10 * Radius * Math.Acos( (b / a))); //?????
            return Sprites[Math.Abs(Index % Sprites.Length)];
        }
        public void Draw2D(ref Graphics G)
        {
            G.DrawEllipse(new Pen(Color.Red, 2), Center.X - Radius, Center.Y - Radius, Radius * 2, Radius * 2);
        }
    }

    public class InverseCircle : Circle
    {
        public InverseCircle(PointF Center, float Radius): base(Center, Radius)
        {
           
        }
        public override double Distance(PointF Location)
        {
            return Radius - Math.Sqrt(Math.Pow(Location.X - Center.X, 2) + Math.Pow(Location.Y - Center.Y, 2));
        }
    }

    public class GameImage : IGameObject
    {
        readonly double Angle;
        readonly int Width = 12;
        public Bitmap Sprite;
        PointF a, b;
        Bitmap[] Sprites;

        public GameImage(Bitmap Img, PointF A, double Angle)
        {
            this.Angle = Angle;
            a = A;

            b = new PointF(A.X + (float)(Math.Cos(Angle) - Width * Math.Sin(Angle)),
                A.Y - (float)(Math.Sin(Angle) - Width * Math.Cos(Angle)));

            Sprite = Img;

            Sprites = new Bitmap[Sprite.Width];
            for (int i = 0, j; i < Sprites.Length; i++)
            {
                Sprites[i] = new Bitmap(1, Sprite.Height);
                for (j = 0; j < Sprite.Height; j++)
                    Sprites[i].SetPixel(0, j, Sprite.GetPixel(i, j));
            }
        }

        public bool IsIntersected(PointF A, PointF B)
        {
            double multiply(float ax, float ay, float bx, float by) => ax * by - bx * ay;

            var v1 = multiply(b.X - a.X, b.Y - a.Y, A.X - a.X, A.Y - a.Y);
            var v2 = multiply(b.X - a.X, b.Y - a.Y, B.X - a.X, B.Y - a.Y);
            var v3 = multiply(B.X - A.X, B.Y - A.Y, a.X - A.X, a.Y - A.Y);
            var v4 = multiply(B.X - A.X, B.Y - A.Y, b.X - A.X, b.Y - A.Y);
            return (v1 * v2 <= 0 && v3 * v4 <= 0);
        }

        public double Distance(PointF Location)
        {
            double t = ((Location.X - a.X) * (b.X - a.X)
                + (Location.Y - a.Y) * (b.Y - a.Y))
              / (Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));

            t = (t > 1) ? 1 : (t < 0) ? 0 : t;

            return Math.Sqrt(
                Math.Pow(a.X - Location.X + (b.X - a.X) * t, 2)
                + Math.Pow(a.Y - Location.Y + (b.Y - a.Y) * t, 2)
                );
        }

        public Bitmap GetSprite(PointF Intersection)
        {
            int Index = (int)(Math.Sqrt(Math.Pow(Intersection.X - a.X, 2) + Math.Pow(Intersection.Y - a.Y, 2)) / (Width + 1) * Sprites.Length);
            return Sprites[Math.Abs(Index)];
        }
        public void Draw(ref Graphics G, PointF P)
        {
            G.DrawLine(new Pen(Color.Green, 2), a, b);
        }
        public void Draw2D(ref Graphics G)
        {
            G.DrawLine(new Pen(Color.Green, 2), a, b);
        }

        public PointF GetIntersection(PointF A, PointF B)
        {
            if (IsIntersected(A, B))
            {
                double x = (b.X * a.Y - a.X * b.Y + (A.X * B.Y - B.X * A.Y) * (b.X - a.X) / (B.X - A.X)) / (a.Y - b.Y + (b.X - a.X) * (B.Y - A.Y) / (B.X - A.X));
                //((a.Y - b.Y) + (b.X - a.X) * (B.X * A.Y - A.X * B.Y) / (B.X - A.X)) / (b.X * a.Y - a.X * b.Y + (b.X - a.Y) * (B.Y - A.Y) / (B.X - A.X));

                double y = (B.X * A.Y - A.X * B.Y + x * (B.Y - A.Y)) / (B.X - A.X);

                return new PointF((float)x, (float)y);
            }
            throw new NotSupportedException("Lines do not cross.");
        }
    }
}
