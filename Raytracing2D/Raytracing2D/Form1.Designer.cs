﻿namespace Raytracing2D
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.trackRayLength = new System.Windows.Forms.TrackBar();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.trackViewArea = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.trackRayCount = new System.Windows.Forms.TrackBar();
            this.checkTextures = new System.Windows.Forms.CheckBox();
            this.checkArea = new System.Windows.Forms.CheckBox();
            this.checkObjects = new System.Windows.Forms.CheckBox();
            this.checkRays = new System.Windows.Forms.CheckBox();
            this.buttonResetLines = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackRayLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackViewArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackRayCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(984, 561);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panelMenu);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.tableLayoutPanel1.SetRowSpan(this.panel2, 2);
            this.panel2.Size = new System.Drawing.Size(978, 555);
            this.panel2.TabIndex = 2;
            // 
            // panelMenu
            // 
            this.panelMenu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelMenu.Controls.Add(this.label9);
            this.panelMenu.Controls.Add(this.label10);
            this.panelMenu.Controls.Add(this.label11);
            this.panelMenu.Controls.Add(this.label12);
            this.panelMenu.Controls.Add(this.trackRayLength);
            this.panelMenu.Controls.Add(this.label8);
            this.panelMenu.Controls.Add(this.label7);
            this.panelMenu.Controls.Add(this.label4);
            this.panelMenu.Controls.Add(this.label5);
            this.panelMenu.Controls.Add(this.label6);
            this.panelMenu.Controls.Add(this.trackViewArea);
            this.panelMenu.Controls.Add(this.label3);
            this.panelMenu.Controls.Add(this.label2);
            this.panelMenu.Controls.Add(this.label1);
            this.panelMenu.Controls.Add(this.trackRayCount);
            this.panelMenu.Controls.Add(this.checkTextures);
            this.panelMenu.Controls.Add(this.checkArea);
            this.panelMenu.Controls.Add(this.checkObjects);
            this.panelMenu.Controls.Add(this.checkRays);
            this.panelMenu.Controls.Add(this.buttonResetLines);
            this.panelMenu.Enabled = false;
            this.panelMenu.Location = new System.Drawing.Point(9, 72);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(194, 396);
            this.panelMenu.TabIndex = 1;
            this.panelMenu.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(70, 239);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(150, 287);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "1000";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 287);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "200";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 239);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Rays length";
            // 
            // trackRayLength
            // 
            this.trackRayLength.Location = new System.Drawing.Point(7, 255);
            this.trackRayLength.Maximum = 1000;
            this.trackRayLength.Minimum = 200;
            this.trackRayLength.Name = "trackRayLength";
            this.trackRayLength.Size = new System.Drawing.Size(178, 45);
            this.trackRayLength.TabIndex = 17;
            this.trackRayLength.Value = 200;
            this.trackRayLength.Scroll += new System.EventHandler(this.RayLengthChanging);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(70, 320);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "label8";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(71, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "label7";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(150, 368);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "360";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 368);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 320);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "View Angle";
            // 
            // trackViewArea
            // 
            this.trackViewArea.Location = new System.Drawing.Point(7, 336);
            this.trackViewArea.Maximum = 360;
            this.trackViewArea.Minimum = 1;
            this.trackViewArea.Name = "trackViewArea";
            this.trackViewArea.Size = new System.Drawing.Size(178, 45);
            this.trackViewArea.TabIndex = 11;
            this.trackViewArea.Value = 1;
            this.trackViewArea.Scroll += new System.EventHandler(this.ViewAreaChanging);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(150, 203);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "1000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "100";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Rays count";
            // 
            // trackRayCount
            // 
            this.trackRayCount.Location = new System.Drawing.Point(7, 171);
            this.trackRayCount.Maximum = 1000;
            this.trackRayCount.Minimum = 100;
            this.trackRayCount.Name = "trackRayCount";
            this.trackRayCount.Size = new System.Drawing.Size(178, 45);
            this.trackRayCount.TabIndex = 7;
            this.trackRayCount.Value = 500;
            this.trackRayCount.Scroll += new System.EventHandler(this.RayCountChanging);
            // 
            // checkTextures
            // 
            this.checkTextures.AutoSize = true;
            this.checkTextures.Checked = true;
            this.checkTextures.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkTextures.Location = new System.Drawing.Point(3, 131);
            this.checkTextures.Name = "checkTextures";
            this.checkTextures.Size = new System.Drawing.Size(99, 17);
            this.checkTextures.TabIndex = 6;
            this.checkTextures.Text = "Enable textures";
            this.checkTextures.UseVisualStyleBackColor = true;
            // 
            // checkArea
            // 
            this.checkArea.AutoSize = true;
            this.checkArea.Checked = true;
            this.checkArea.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkArea.Location = new System.Drawing.Point(3, 108);
            this.checkArea.Name = "checkArea";
            this.checkArea.Size = new System.Drawing.Size(77, 17);
            this.checkArea.TabIndex = 5;
            this.checkArea.Text = "Show area";
            this.checkArea.UseVisualStyleBackColor = true;
            // 
            // checkObjects
            // 
            this.checkObjects.AutoSize = true;
            this.checkObjects.Checked = true;
            this.checkObjects.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkObjects.Location = new System.Drawing.Point(3, 85);
            this.checkObjects.Name = "checkObjects";
            this.checkObjects.Size = new System.Drawing.Size(90, 17);
            this.checkObjects.TabIndex = 4;
            this.checkObjects.Text = "Show objects";
            this.checkObjects.UseVisualStyleBackColor = true;
            // 
            // checkRays
            // 
            this.checkRays.AutoSize = true;
            this.checkRays.Location = new System.Drawing.Point(3, 62);
            this.checkRays.Name = "checkRays";
            this.checkRays.Size = new System.Drawing.Size(75, 17);
            this.checkRays.TabIndex = 3;
            this.checkRays.Text = "Show rays";
            this.checkRays.UseVisualStyleBackColor = true;
            // 
            // buttonResetLines
            // 
            this.buttonResetLines.Location = new System.Drawing.Point(3, 9);
            this.buttonResetLines.Name = "buttonResetLines";
            this.buttonResetLines.Size = new System.Drawing.Size(182, 47);
            this.buttonResetLines.TabIndex = 2;
            this.buttonResetLines.Text = "Generate walls";
            this.buttonResetLines.UseVisualStyleBackColor = true;
            this.buttonResetLines.Click += new System.EventHandler(this.buttonResetLines_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(777, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(192, 184);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(978, 555);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Raytracing2D";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackRayLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackViewArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackRayCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button buttonResetLines;
        private System.Windows.Forms.CheckBox checkArea;
        private System.Windows.Forms.CheckBox checkObjects;
        private System.Windows.Forms.CheckBox checkRays;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox checkTextures;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TrackBar trackViewArea;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trackRayCount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TrackBar trackRayLength;
    }
}

